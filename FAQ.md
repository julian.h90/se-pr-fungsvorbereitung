# FAQ

## **Allgemein**

### *Nennen Sie ein Programmierwerkzeug für die statische Codeanalyse*:
> PMD

### *Was ist ein Build Management Tool und wofür braucht man es?*:
> Gewährleistet, dass genau die hinterlegten Projektabhängigkeiten, wie Libraries und Java-Versionen, verwendet werden. Maven, Gradle sind Build Tools.

### *Nennen Sie zwei Vorteile von Build Management Tools*:
> * Build Prozess wird automatisiert
> * Abhängigkeiten werden automatisch heruntergeladen und hinzugefügt

## **Tests**

### *Tests allgemein (keine Frage)*:
> Tests können generell nur die Anwesenheit von Fehlern aufzeigen, nicht ihre Abwesenheit

### *Verifikation*:
> Erstellen wir das Programm richtig? Die Software erfüllt alle funktionale und nicht-funktionale Anforderungen des Kunden.

### *Validierung*:
> Erstellen wir das richtige Produkt? Die Software entspricht den Vorstellungen und Anforderungen des Kunden. "Er bekommt das, was er bestellt hat." (Gegenbeispiel: Software arbeitet nicht so, wie sich der Kunde das vorgestellt hat (obwohl funktionale Anforderungen erfüllt sind))

### *Was sind funktionale/nicht-funktionale Anforderungen*:
> Funktionale Anforderungen beschreiben gewünschte Funktionalitäten (was soll das System tun/können) eines Systems bzw. Produkts, dessen Daten oder Verhalten.

> Nichtfunktionale Anforderungen sind Anforderungen, an die "Qualität" in welcher die geforderte Funktionalität zu erbringen ist. z.B. Verfügbarkeit (24/7), Reaktionszeit/Responsetime, Zugriffsberechtigungen

### *Testautomatisierung*:
> Tests werden automatisch vor jedem Commit ausgeführt, um nur stabile Versionen zu gewährleisten.

### *Pfadabdeckungstest*:
> Der Pfadabdeckungstest ist ein White-Box Test. Er testet jeden unabhängigen Ausführungspfades eines Programms (sprich jedes if und jedes else muss separat getestet werden)

### *Regressionstests*:
> Bei neuen Implementierungen müssen sämtliche alte Tests funktionieren. Wird durch automatisierte Tests erleichtert. Können mögliche ungewollte Nebeneffekte aufdecken.

### *Systemtests*:
> Systemtests werden von den Entwicklern ausgeführt und sind Fehlertests (Testen von Funktionen/Methoden/Mocking usw.). Zudem überprüfen Sie das Zusammenspiel der einzelnen Komponenten (emergentes Verhalten) und deren Interaktion miteinander sowie Korrektheit und Kompatibilität.

### *Freigabetest (Release Test)*:
> Der Freigabetest erfolgt durch ein separates Team, das nicht an der Entwicklung beteiligt war (nicht voreingenommen usw). Ziel ist testen der Funktionalität, Zuverlässigkeit und Performance. Der Freigabetest ist ein Black-Box Test.

### *Whitebox-Test*:
> Testen mit Zugriff auf den Quellcode. (durch Unit-tests) 

### *Blackbox-Test*:
> Testen ohne Zugriff auf den Quellcode (z.B. Anwendung im Browser testen).

### *Benutzertests*:
> Sind Alphatests, Betatests und Abnahmetests. Bei Alphatests arbeiten Benutzer mit dem Entwicklungsteam zusammen, um die Software in der Entwicklungsumgebung zu testen. Bei Betatests werden den Benutzern ein Release der Software zum experimentieren zur Verfügung gestellt. Bei Abnahmetests testet der Kunde das System und entscheidet, ob die Software in deren Umgebung installiert werden soll.

### *Nennen Sie die 6 Phasen des Abnahmetests*:
> 1. Abnahmekriterien festlegen
> 2. Abnahmetests planen
> 3. Abnahmetests ableiten
> 4. Abnahmetests ausführen
> 5. Testergebnisse verhandlen
> 6. Das System ablehnen bzw. akzeptieren

### *Leistungstests*:
> Leistungstests ist ein Teil des Freigabe tests, bei denen Performancetests und Lasttests durchgeführt werden (siehe nicht-funktionale Anforderungen). Bei Performancetests werden die Antwortzeit mit unterschiedlichen Datenmengen gemessen sowie die Skalierbarkeit (Performance bei steigender Last). Bei Lasttests werden Grenzen (z. B. bestimmte Anzahl von Zugriffen) getesten, die weit über den festgelegten Anforderungen liegen.

### *Anforderungsbasiertes Testen*:
> Testen aufgrund der gestellten Anforderungen des Kunden. Für jede Anforderung wird eine separate Testreihe aufgestellt.

### *Szenariobasiertes Testen*:
> Testen anhand typischer Benutzerszenarien. Szenario ist eine Geschichte, die eine Möglichkeit beschreibt, das System zu nutzen. z.B. Patient anlegen und Röntgenaufnahme hinzufügen

### *Was ist Softwareinspektion*:
> Softwareinspektion sind Reviews zur Analyse und Prüfung von Systemanforderungen, Entwurfsmodelle, Quellcode(Code-Review), Datenbankschemata und Systemtests

### *Was sind Vorteile Softwareinspektion*:
> * Können verdeckte Fehler aufdecken
> * Kostengünstig
> * Betrachtung von qualitativen Programmeigenschaften

### *Welche unterschiedliche Schnittstellen können getestet werden*:
> * Parameterschnittstellen
> * Schnittstellen mit gemeinsamen Speicher
> * Prozedurschnittstellen
> * Schnittstellen zur Nachrichtenübergabe

### *Nennen Sie 3 Schnittstellenfehler*:
> * Falsche Verwendung der Schnittstelle
> * Schnittstellenmissverständnis
> * Zeitabstimmungsfehler

### *Anwendungsfalltesten*:
> * Testen von Use-Cases (z.B. Anlegen eines Patienten)
> * Use-Case beinhalten idR mehrere Systemkomponenten

### *Nennen Sie 2 Richtlinen zum Anwendungsfalltesten*:
> * Test aller Funktionen die über das Menü erreichbar sind
> * Test aller Benutzereingaben mit richtig und falschen Werten

## **REST**

### *Was bedeutet RESTful und deren Constraints*:
> Representational State Transfer (REST) ist eine einfache, zustandslose Architektur, die über HTTP läuft. 

> Folgende Constraints müssen für einen gültigen REST Service erfüllt sein:
> * Uniform Interface (Einheitliche Schittstelle)
> * Stateless (Zustandslos)
> * Cacheable (Zwischenspeicherbar)
> * Client-Server (Trennung UI <> Server)
> * Layered System (z.B. Load Balancing)
> * (Code on Demand)

### *Was bietet ein RESTful Service*:
> * Skalierbarkeit
> * Portabilität
> * Anpassbarkeit
> * Einfachheit
> * Zuverlässigkeit

### *Was bedeutet Ressourcenbasiert*:
> Eine Ressource stellt eine Informationsquelle dar, die mit einem URI adressiert werden kann.

### *Was bedeutet Repräsentation*:
> Der Server gibt Repräsentationen der Information an den Client (HTML, XML, JSON) zurück.

### *Was bedeutet stateless*:
> Weder der Server noch die Anwendung soll Zustandsinformationen zwischen zwei Nachrichten speichern.

### *Was bedeutet Cacheable*:
> Serverresponses können lokal zwischengespeichert werden auf welche bei wiederholenden Requests zugegriffen wird (anstelle des Sendens eines Requests an den Server).

> Vorteil:
> * Performance und Skalierbarkeit

> Nachteil:
> * Gefahr der Nutzung von veralteten Daten

### *Was ist bei der Fehlerbehandlung einer REST API zu beachten?*:
> * Fehler explizit behandeln
> * Technologieunabhängige Fehlermeldung
> * Feedback über passende HTTP Status Codes
> * Standardisierte Fehlermeldungen an Client (z.B. Error Code, Error Message)
> * Dokumentation

## **ORM**

### *Was ist ein Objekt relational mapping*:
> Übersetzung eines Objektes in eine Relationale Struktur. Sprich, Ein Java Objekt wird mit dessen Attribute in ein gülties SQL Table umgewandelt. Sprich, für jedes Attribut eine Spalte/Column.

### *Nennen Sie ein Beispiel für einen Objekt relational Mapper*:
> Hibernate, Eclipse Link

### *Nennen Sie jeweils 2 Vor- und Nachteile eines ORM*:
> Vortel:
> * Schnellere Entwicklung (Entwickler konzentrieren sich nur auf den Application Code)
> * Datenbankunabhängig

> Nachteil:
> * Performancenachteile
> * Generiertes SQL nicht für jeden Anwendungsfall optimal

### *Was bedeutet JPA*:
> Java Persistence API. Definiert die Schnittstellenspezifikationen zur Kommunikation von Objekten mit relationalen Datenbanken. (z.B. @Column, @OneToMany)

### *Nenne mehrere HTTP-Response Codes*:
> * 200 - Ok (200er = Success)
> * 404 - Not found (400er = Client Fehler)
> * 500 - Internal Server Error (500er = Server Fehler)

## **Serviceorientierte Architektur**

### *Erläutere Service orientierte Architektur*:
> ~~SOA besteht aus Service Engineering und Softwareentwicklung mit Services. Das Ziel ist eine flexible Zusammenarbeit einzelner Server. Einzelne Services sind unabhängig von Plattform (z.B. Betriebssystem) und Programmiersprache~~
> SOA ist ein Architekturmuster basierend auf einzeln gekapselten Diensten und deren Orchestrierung zu einem "höheren" Dienst, der dem User zur Verfügung gestellt wird. Dieser Dienst richtet sich dabei primär nach einem Geschäftsprozess (Patienten anlegen)
> <https://de.wikipedia.org/wiki/Serviceorientierte_Architektur>

### *Was sind die Vorteile von SOA*:
> * Programmiersprachenunabhängig
> * Einfacher Informationsaustausch
> * Investitionen in Legacy Systeme können erhalten werden

### *Was bedeutet Service Engineering?*:
> Implementierung von unabhängigen, wiederverwendbaren Services.

### *Was bedeutet Softwareentwicklung mit Services?*:
> Entwicklung einer Anwendung unter Verwendung mehrere Services

## **Aspektorientierte Programmierung**

### *Erklären Sie die Aspektorientierte Programmierung*:
> Der Entwicklungsansatz bezieht sich auf einen "Aspekt" bzw. Belange. Ein Aspekt kapselt die Querschnittsbelange bzw. Querschnitts-Funktionalitäten (cross-cutting-concerns) von verschiedenen (Code-) Stellen separat (in einer Klasse).

> Der Aspekt definiert, wo und wie er im Programmcode eingebunden werden soll

### *Nennen Sie ein typisches Beispiel der Aspektorientierten Programmierung*:
> Logging

### *Was bedeutet Seperation of concerns?*:
> Jedes Programmelement erledigt genau eine Aufgabe

### *Was bedeutet Cross-cutting-concerns*:
> Aspekte kapseln Querschnittsbelange (sprich in einer separaten Klasse)

### *Was ist der Aspect Weaver bzw. was bedeutet Aspect weaving*:
> Der Aspect Weaver "webt" die Aspekte an den bestimmten Stellen (Point-Cuts) in das Programm ein. Das geschieht bei der Kompilierung des Programms.

### *Nenne die 4 Bestandteile, die bei der AOP verwendet werden*:
> * Aspect: Implementierter Querschnittsbelang und Definition des Pointcuts
> * Pointcut: Definiert, an welchen Join Points der Advice ausgeführt werden soll (z.B. Execution)
> * Join Point: die Stelle, an der der Advice ausgeführt werden soll (z.B. Package-Pfad)
> * Advice: Code, der den Belang umsetzt (Sprich, die Methode die Ausgeführt werden soll)

## **Design Patterns**

### *Definieren Sie den Begriff Design Patterns*:
> Definieren allgemeine gültige Lösungsansätze für wiederkehrende Problemstellungen in der Softwareentwicklung

### *Nennen Sie die drei verschiedenen Entwurfsmuster von Design Patterns*:
> * Erzeugungsmuster = Prozesse zur Objekterzeugung (Singleton, Abstract Factory)
> * Strukturmuster = Zusammenfassung von Klassen/Objekten zu größeren Strukturen (Adapter)
> * Verhaltensmuster = Interaktion zwischen Objekten (Observer)

### *Nennen Sie 3 unterschiedliche konkrete Beispiele von Design Patterns und erläutern Sie eines davon*:
> * Singleton (nur ein Objekt zur Laufzeit)
> * Adapter (Anpassung einer Schnittstelle zur Zusammenarbeit von Klassen)
> * Observable (Automatische Info über Zustandsänderungen an Objekten via Publish und Subscribe (z.B. Event Handler, Listener))
> * Abstract Factory (eine Anwendung auf verschiedenen Plattformen; Erstellung von Objekten erfolgt über abstrakte Objekte (nicht über konkrete Objekte))

## **Softwareevolution**

### *Erklären Sie den Begriff Technische Schulden*:
> Schlecht entwickelte Software bringt unwiederruflich hohe Kosten mit sich z.b. Wartungskosten, Fehlerbehebung usw., die bei gut entwickelter Software nicht aufgetreten wären

### *Nennen Sie 3 Gründe warum Softwareveränderungen unvermeidbar sind*:
> * Neue Anforderungen
> * Umgebungsänderungen
> * Fehlerbehebung

### *Nennen Sie 3 Faktoren, die die Wartungskosten beeinflussen*:
> * Fehlerbehebung
> * Implementierung erweiternder Funktionen
> * Änderung der Umgebung (Betriebssystem, Hardware)

### *Was ist die Dynamik der Programmevolution*:
> Untersuchung von Systemänderungen und damit verbundener Prozesse -> Entstehung der Lehmanns Gesetze

### *Nennen Sie 3 Lehmanns Gesetze*:
> Lehmanns Gesetze sind keine Gesetze sondern Beobachtungen

> * Kontinuierliche Veränderung = System muss sich stetig der Umgebung anpassen (sonst Nutzen-Verlust)
> * Kontinuierliches Wachstum = Funktionsumfang muss stetig wachsen (für Kundenzufriedenheit)
> * Bewahrung der Vertrautheit = Umfang der vorgenommenen Änderungen für jede neue Version ist nahezu Konstant (ähnlich-große Releases/neue Funktionanzahl)

### *Erläutern Sie Reengineering-Prozess*:
> Re-strukturieren bzw. neu schreiben von Teilen eines Systems. Sinnvoll, wenn nicht alle Subsysteme regelmäßig gewartet werden müssen

### *Nennen Sie 2 Vorteile von Reengingeering*:
> * Geringeres Risiko gegenüber einer Neuentwicklung
> * Geringere Kosten gegenüber einer Neuentwicklung

### *Nennen Sie 3 mögliche Aktivitäten im Reengineering-Prozess*:
> * Übersetzung Quellcodes (in neuere Programmiersprache)
> * Reverse Engineering (Analyse des Programms)
> * Verbesserung der Programmqualität (Bessere Lesbarkeit/Verständnis)
> * Modularisierung (Code-Struktur anpassen; kann Refactoring der Architektur erfordern)
> * Daten-Reengineering (Kann Veränderung der Datenbankschemata erforderlich machen)

### *Wie wirkt sich Refactoring auf die Wartung einer Software aus*:
> * Komplexität reduzieren
> * Struktur verbessern
> * Verständlichkeit erhöhen
> * allg. Programmverbesserungen (aber keine neuen Funktionalitäten)
> * Präventive Wartung, um zukünftige Änderungen zu erleichtern

### *Was sind Bad Smells*:
> Situationen in denen der Code verbessert werden kann
> * Code-Duplizierungen
> * lange Methoden
> * Switch-Case Anweisungen
> * Datenklumpen

### *Woran erkennt man das man eine Software eher Re-Engineeren sollte und wann Refactoren*:
> Wenn die Wartungskosten der Software höher sind als die Initialkosten zur Neuentwicklung der Software -> Reengineering; sonst Refactoren

### *Wann findet Refactoring statt*:
> Refactoring findet kontinuierlich während des gesamten Entwicklungs- und Evolutionsprozesses statt

### *Nennen Sie 4 Strategien zum Umgang mit Legacy Systemen*:
> 1. System ausmustern
> 2. System unverändert lassen und regelmäßig warten
> 3. System per Reengineering sanieren
> 4. System ganz oder teilweise durch ein neues System ersetzen

## **Konfigurationsmanagement**

### *Was ist das Konfigurationsmanagement*:
> Konfigurationsmanagement kümmert sich mit Hilfe des Change Management, Versionsmanagement, Systemerstellung und Release Management um das Managen sich ändernder Software Systeme.

### *Was macht das Change-Management*:
> * Bewertung von Change-Requests (Änderungsanforderungen) bezgl. Kosten und Auswirkungen
> * Priorisierung von Change-Requests (nach Dringendsten, Wichtigsten, Kosten-Nutzen)
> * Entscheidung zur Implementierung der Change-Requests

### *Was macht das Versionsmanagement*:
> * Verwaltung verschiedene Systemversionen
> * Vermeidung von Versionskonflikten
> * Mainline > Baseline > Codeline (Mainline besteht aus Baseline usw.)

### *Was macht das Systemerstellung*:
> * Erstellung eines ausführbaren Systems
> * Zusammensetzung aus Komponenten, Daten und Bibliotheken

### *Was macht das Release Management*:
> * Vorbereitung für externe Releases der Software
> * Major Releases = Signifikante neue Features
> * Minor Releases = Fehlerbehebungen, kleinere Änderungen

## **TDD**

### *Was ist TDD*:
> Test-Driven-Development ist ein Ansatz um Software zu entwicklen. Dabei werden vor Implementierung von Features zuerst dazugehörige Tests geschrieben. Erst wenn diese Tests, sowie sämtliche Tests davor, erfolgreich sind, darf das nächste Feature/Inkrement implementiert werden.

### *Nennen Sie 4 Vorteile von TDD*:
> * Codeabdeckung - für jedes Codesegment gibt es mindestens einen Test
> * Regressionstests - die Testabdeckung/-anzahl/-qualität wird stetig erhöht
> * Vereinfachtes Fehlerbeheben - wenn ein Test fehlschlägt, ist es offensichtlich wo das Problem ist
> * Systemdokumentation - Tests sind eine Art Dokumentation