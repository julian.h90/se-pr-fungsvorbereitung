# FAQ

## **Allgemein**

### *Nennen Sie ein Programmierwerkzeug für die statische Codeanalyse*:
> _

### *Was ist ein Build Management Tool und wofür braucht man es?*:
> _

### *Nennen Sie zwei Vorteile von Build Management Tools*:
> _

## **Tests**

### *Tests allgemein (keine Frage)*:
> _

### *Verifikation*:
> _

### *Validierung*:
> _

### *Was sind funktionale/nicht-funktionale Anforderungen*:
> _

### *Testautomatisierung*:
> _

### *Pfadabdeckungstest*:
> _

### *Regressionstests*:
> _

### *Systemtests*:
> _

### *Freigabetest (Release Test)*:
> _

### *Whitebox-Test*:
> _

### *Blackbox-Test*:
> _

### *Benutzertests*:
> _

### *Nennen Sie die 6 Phasen des Abnahmetests*:
> _


### *Leistungstests*:
> _

### *Anforderungsbasiertes Testen*:
> _

### *Szenariobasiertes Testen*:
> _

### *Was ist Softwareinspektion*:
> _

### *Was sind Vorteile Softwareinspektion*:
> _

### *Welche unterschiedliche Schnittstellen können getestet werden*:
> _

### *Nennen Sie 3 Schnittstellenfehler*:
> _

### *Anwendungsfalltesten*:
> _

### *Nennen Sie 2 Richtlinen zum Anwendungsfalltesten*:
> _

## **REST**

### *Was bedeutet RESTful und deren Constraints*:
> _

### *Was bietet ein RESTful Service*:
> _


### *Was bedeutet Ressourcenbasiert*:
> _

### *Was bedeutet Repräsentation*:
> _

### *Was bedeutet stateless*:
> _

### *Was bedeutet Cacheable*:
> _

### *Was ist bei der Fehlerbehebung einer REST API zu beachten?*:
> _

## **ORM**

### *Was ist ein Objekt relational mapping*:
> _

### *Nennen Sie ein Beispiel für einen Objekt relational Mapper*:
> _

### *Nennen Sie jeweils 2 Vor- und Nachteile eines ORM*:
> _

### *Was bedeutet JPA*:
> _

### *Nenne mehrere HTTP-Response Codes*:
> _

## **Serviceorientierte Architektur**

### *Erläutere Service orientierte Architektur*:
> _

### *Was sind die Vorteile von SOA*:
> _

### *Was bedeutet Service Engineering?*:
> _

### *Was bedeutet Softwareentwicklung mit Services?*:
> _

## **Aspektorientierte Programmierung**

### *Erklären Sie die Aspektorientierte Programmierung*:
> _

### *Nennen Sie ein typisches Beispiel der Aspektorientierten Programmierung*:
> _

### *Was bedeutet Seperation of concerns?*:
> _

### *Was bedeutet Cross-cutting-concerns*:
> _

### *Was ist der Aspect Weaver bzw. was bedeutet Aspect weaving*:
> _

### *Nenne die 4 Bestandteile, die bei der AOP verwendet werden*:
> _

## **Design Patterns**

### *Definieren Sie den Begriff Design Patterns*:
> _

### *Nennen Sie die drei verschiedenen Entwurfsmuster von Design Patterns*:
> _

### *Nennen Sie 3 unterschiedliche konkrete Beispiele von Design Patterns und erläutern Sie eines davon*:
> _

## **Softwareevolution**

### *Erklären Sie den Begriff Technische Schulden*:
> _

### *Nennen Sie 3 Gründe warum Softwareveränderungen unvermeidbar sind*:
> _

### *Nennen Sie 3 Faktoren, die die Wartungskosten beeinflussen*:
> _

### *Was ist die Dynamik der Programmevolution*:
> _

### *Nennen Sie 3 Lehmanns Gesetze*:
> _

### *Erläutern Sie Reengineering-Prozess*:
> _

### *Nennen Sie 2 Vorteile von Reengingeering*:
> _

### *Nennen Sie 3 mögliche Aktivitäten im Reengineering-Prozess*:
> _

### *Wie wirkt sich Refactoring auf die Wartung einer Software aus*:
> _

### *Was sind Bad Smells*:
> _

### *Woran erkennt man das man eine Software eher Re-Engineeren sollte und wann Refactoren*:
> _

### *Wann findet Refactoring statt*:
> _

### *Nennen Sie 4 Strategien zum Umgang mit Legacy Systemen*:
> _

## **Konfigurationsmanagement**

### *Was ist das Konfigurationsmanagement*:
> _

### *Was macht das Change-Management*:
> _

### *Was macht das Versionsmanagement*:
> _

### *Was macht das Systemerstellung*:
> _

### *Was macht das Release Management*:
> _

## **TDD**

### *Was ist TDD*:
> _

### *Nennen Sie 4 Vorteile von TDD*:
> _
